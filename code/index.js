import "./index.css";

class ObservableModel {
  constructor() {
    this.state = {
      horse1: 0,
      horse2: 0,
      poo: 0
    };

    this.observers = [];
  }

  subscribe(fn) {
    this.observers.push(fn);
  }

  update(newState) {
    const updatedState = Object.keys(newState).reduce((acc, key) => {
      if (newState[key] < 0) {
        return { ...acc, [key]: 0 };
      }

      return { ...acc, [key]: newState[key] };
    }, {});
    this.state = { ...updatedState };
    this.observers.forEach(fn => fn(this.state));
  }

  getState() {
    return { ...this.state };
  }
}

const model = new ObservableModel();

const getHorse = (pic = "🦄", key) => {
  const template = `
  <div class="horse">
    <div class="img">${pic}</div>
    <button class="plus">+</button><input type="text" value="0"/> <button class="minus">-</button>
  </div>`;

  const container = document.createElement("div");
  container.innerHTML = template;
  container.querySelector(".plus").addEventListener("click", () => {
    const state = model.getState();
    model.update({ ...state, [key]: state[key] + 1 });
  });

  container.querySelector(".minus").addEventListener("click", () => {
    const state = model.getState();
    model.update({ ...state, [key]: state[key] - 1 });
  });

  model.subscribe(newValue => {
    const state = model.getState();
    container.querySelector("input").value = state[key];
  });

  return container;
};

const getFormula = () => {
  const { horse1, horse2, poo } = model.getState();
  const template = `
  <div class="formula">
    <span>${horse1}</span> +  <span>${horse2}</span> + ${poo} = ${horse1 +
    horse2 +
    poo}
  </div>`;

  const container = document.createElement("div");
  container.innerHTML = template;
  model.subscribe(newValue => {
    container.querySelector(".formula").innerHTML = `<span>${
      newValue.horse1
    }</span> +  <span>${newValue.horse2} </span> + ${
      newValue.poo
    } = ${newValue.horse1 + newValue.horse2 + newValue.poo}`;
  });
  return container;
};

const getFormula1 = () => {
  const { horse1, horse2, poo } = model.getState();
  const template = `
  <div class="formula">
    <span>${horse1}</span> +  <span>${horse2}</span> + ${poo} = ${horse1 +
    horse2 +
    poo}
  </div>`;

  const container = document.createElement("div");
  container.innerHTML = template;
  model.subscribe(newValue => {
    container.querySelector(".formula").innerHTML = `<span>${
      newValue.horse1
    }</span> +  <span>${newValue.horse2} </span> + ${
      newValue.poo
    } = ${newValue.horse1 - newValue.horse2 - newValue.poo}`;
  });
  return container;
};

const drawHorse = (content, selector = ".showcase") => {
  document.querySelector(selector).appendChild(content);
};

const drawFormula = (content, selector = ".formulaContainer") => {
  document.querySelector(selector).appendChild(content);
};

drawHorse(getHorse("🦄", "horse1"));
drawHorse(getHorse("🐴", "horse2"));
drawHorse(getHorse("💩", "poo"));
drawFormula(getFormula());
drawFormula(getFormula());
drawFormula(getFormula());
drawFormula(getFormula());
drawFormula(getFormula());
drawFormula(getFormula1());
