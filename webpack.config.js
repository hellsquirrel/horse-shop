const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./code/index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname)
  },
  mode: "development",
  devServer: {
    hot: true
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.css$/,
        use: [{ loader: "style-loader" }, { loader: "css-loader" }]
      }
    ]
  }
};
